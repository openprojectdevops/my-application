# My-application

## 0. Infos
Source du code my-application:  
https://github.com/bezkoder/angular-11-spring-boot-postgresql.git  

Documents lus:  
- Angular2Guy sur github  
- Documentations utilses pour ce mini projet:  
- https://docs.k0sproject.io/v1.25.2+k0s.0/examples/nginx-ingress/  
- https://platform9.com/learn/v1.0/tutorials/nodeport-ingress  


## 1. Setup
### a. Env local
 - Installer maven 3.6.2 minimum
 - Java 17
 - Install node js/npm

### b. Env k8s
 - Construire votre cluster k8s avec vagrant si **besoin** ou utiliser le votre, voir le [readme](/env-k8s-cluster/README.md) (celui utilise pour les tests sur ce projet - pris sur internet)


## 2. Deployer localement

### a. PostgreSQL
  - Aller dans le repertoire **postgresql** et executer le scripts ./runDocker.sh

### b. Backend
  - Aller dans le repertoire **backend** et executer le scritps ./runDocker.sh

### c. Front
  - Aller dans le repertoire **front** et executer le scritps ./runDocker.sh


## 3. Deployer sur un cluster kubernetes avec helm
  ### a. Helm 
  - Installer l'outil helm sur le cluster et configurer si non existant
  ### b. Installer ingress-controller
  ```
  helm repo add ingress-nginx https://kubernetes.github.io/ingress-nginx
  helm repo update
  helm install mon-ingress-controller ingress-nginx/ingress-nginx

  ```
  * Pour un cluster sans cloud provider (loadbalance), executer
  ```
  kubectl get -A ValidatingWebhookConfiguration
  kubectl delete -A ValidatingWebhookConfiguration mon-ingress-controller-ingress-nginx-admission
  
  ```
  * le fichier *ingress-nginx-with-nodeport* cree un service the type nodeport (Pour un cluster heberge sur un cloud provider, ne rien faire de plus)
  ```
  cd helm
  kubectl apply -f ingress-nginx-with-nodeport.yaml  
  ```

  ### c. Regles de l'ingress pour l'application 'my-application'
  ```
  cd helm
  kubectl apply -f my-application-with-ingress.yml
  ```
### d. Cas d'un cluster non heberge - config du DNS

* Inserer les lignes suivantes dans la fichier /etc/hosts
- Changer **[NODE IP]** par l'adresse ip de votre service ingress-nginx nodeport (3. b.)
```
[NODE IP]   my-db-test.io

Dans le cas ou nous avons d'autres regles crees dans le rule file de l'ingress:
[NODE IP]   my-front-test.io
[NODE IP]   my-backend-test.io
```

 ### e. Deployer l'application 'my-application'
 ```
 cd helm/my-application-v1
 helm package .
 helm install myapplication myapplication-0.1.0.tgz 
 ```
Utiles:
Images docker sur le registry public:
 ```
 docker pull ngnkmkmt/front:latest
 docker pull ngnkmkmt/backend:latest
```





