#!/bin/sh
#./mvnw clean install -Ddocker=true -Dnpm.test.script=test-chromium
./mvnw clean package -DskipTests
docker build -t ngnkmkmt/backend:latest --build-arg JAR_FILE=spring-boot-jpa-postgresql-0.0.1-SNAPSHOT.jar --no-cache .
docker run --name myapp-backend -e SPRING_DATASOURCE_PASSWORD=M3P@ssw0rd\! -e SPRING_DATASOURCE_USERNAME=myapplication -e SPRING_DATASOURCE_URL=jdbc:postgresql://localhost:5432/myapp -p 8080:8080 --memory="1g" --network="host" ngnkmkmt/backend:latest
