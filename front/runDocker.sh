#!/bin/sh
#./mvnw clean install -Ddocker=true -Dnpm.test.script=test-chromium
docker build -t ngnkmkmt/front:latest --no-cache .
docker run --name myapp-front -d -p 80:80 --network="host" ngnkmkmt/front:latest
